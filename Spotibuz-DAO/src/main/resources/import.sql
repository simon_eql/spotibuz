INSERT INTO `member` (name, surname, login, password, photo, email) VALUES ('Simon', 'Harnois', 'simon', 'a1z2e3r4t5y6', './resources/pictures/2-Simon_HARNOIS_pic_b.png', 'simon@gmail.com');
INSERT INTO `member` (name, surname, login, password, photo, email) VALUES ('Mélo', 'Mane', 'melo', 'b1z2e3r4t5y6', './resources/pictures/2-Mélo_MANE_pic_b.png', 'mélo@gmail.com');

INSERT INTO `artist` (name, photo, active_years, nb_albums) VALUES ('John Lennon','1-John_Lennon.jpg', '1957-1975, 1980', 3);
INSERT INTO `artist` (name, photo, active_years, nb_albums) VALUES ('The Beatles','1-The_Beatles.jpeg', '1962-1970', 1);
INSERT INTO `artist` (name, photo, active_years, nb_albums) VALUES ('Pink Floyd','1-Pink_Floyd.jpg', '1965-1995, 2005, 2014', 1);
INSERT INTO `artist` (name, photo, active_years, nb_albums) VALUES ('Piano covers musician', '1-Piano covers musician.jpg', 'Depuis 2020', 1);

INSERT INTO `album` (zip_file, name, artist_id, photo, release_date, nb_tracks, album_length, sampling_frequency, dynamic_range) VALUES ('', 'Double Fantasy Stripped Down', 1, '0-COVER_Double_Fantasy_Stripped_Down.jpg', '17 novembre 1980', 14, '1 h 33 min 05 s', 44.1, 16);
INSERT INTO `album` (zip_file, name, artist_id, photo, release_date, nb_tracks, album_length, sampling_frequency, dynamic_range) VALUES ('', 'Imagine', 1, '0-COVER_Imagine.jpg', '9 septembre 1971', 10, '39 min 37 s', 96.0, 16);
INSERT INTO `album` (zip_file, name, artist_id, photo, release_date, nb_tracks, album_length, sampling_frequency, dynamic_range) VALUES ('', 'Plastic Ono Band', 1, '0-COVER_Plastic_Ono_Band.jpg', '11 décembre 1970', 11, '39 min 33 s', 96.0, 24);
INSERT INTO `album` (zip_file, name, artist_id, photo, release_date, nb_tracks, album_length, sampling_frequency, dynamic_range) VALUES ('', 'Abbey Road', 2, '0-COVER_Abbey_Road.jpg', '26 septembre 1969', 17, '47 min 20 s', 96.0, 24);
INSERT INTO `album` (zip_file, name, artist_id, photo, release_date, nb_tracks, album_length, sampling_frequency, dynamic_range) VALUES ('', 'Wish You Were Here', 3, '0-COVER_Wish_You_Were_Here.jpg', '15 septembre 1975', 5, '44 min 11 s', 96.0, 24);
INSERT INTO `album` (zip_file, name, artist_id, photo, release_date, nb_tracks, album_length, sampling_frequency, dynamic_range) VALUES ('./resources/albumFiles/Lennon & Beatles covers.zip', 'Lennon & Beatles covers', 4, '0-COVER_Lennon and Beatles covers_b.jpg', '2021', 4, '8 min 09 s', 44.1, 16);

INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 1, '(Just Like) Starting Over', 1, "04:24");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 2, 'Kiss Kiss Kiss', 1, "02:44");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 3, 'Cleanup Time', 1, "03:56");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 4, 'Give Me Something', 1, "01:31");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 5, 'I m Losing You', 1, "04:25");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 6, 'I m Moving On', 1, "02:28");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 7, 'Beautiful Boy (Darling Boy)', 1, "03:50");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 8, 'Watching The Wheels', 1, "03:32");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 9, 'Yes, I m Your Angel', 1, "02:53");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 10, 'Woman', 1, "03:45");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 11, 'Beautiful Boys', 1, "03:15");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 12, 'Dear Yoko', 1, "03:03");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 13, 'Every Man Has A Woman Who Loves Him', 1, "04:45");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 14, 'Hard Times Are Over', 1, "03:37");

INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 1, 'Imagine', 2, "02:59");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 2, 'Crippled Inside', 2, "03:43");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 3, 'Jealous Guy', 2, "04:10");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 4, 'It s So Hard', 2, "02:22");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 5, 'I Don t Want To Be A Soldier', 2, "06:01");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 6, 'Give Me Some Truth', 2, "03:11");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 7, 'Oh My Love', 2, "02:40");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 8, 'How Do You Sleep?', 2, "05:29");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 9, 'How?', 2, "03:37");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 10, 'Oh Yoko!', 2, "04:18");

INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 1, 'Mother', 3, "05:35");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 2, 'Hold On', 3, "01:52");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 3, 'I Found Out', 3, "03:37");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 4, 'Working Class Hero', 3, "03:47");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 5, 'Isolation', 3, "02:52");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 6, 'Remember', 3, "04:32");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 7, 'Love', 3, "03:22");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 8, 'Well Well Well', 3, "05:57");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 9, 'Look At Me', 3, "02:53");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 10, 'God', 3, "04:12");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 11, 'My Mummy s Dead', 3, "00:51");

INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 1, 'Come Together', 4, "05:35");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 2, 'Something', 4, "05:35");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 3, 'Maxwell s Silver Hammer', 4, "05:35");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 4, 'Oh! Darling', 4, "05:35");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 5, 'Octopus s Garden', 4, "05:35");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 6, 'I Want You (She s So Heavy)', 4, "05:35");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 7, 'Here Comes the Sun', 4, "05:35");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 8, 'Because', 4, "05:35");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 9, 'You Never Give Me Your Money', 4, "05:35");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 10, 'Sun King', 4, "05:35");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 11, 'Mean Mr. Mustard', 4, "05:35");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 12, 'Polythene Pam', 4, "05:35");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 13, 'She Came In Through the Bathroom Window', 4, "05:35");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 14, 'Golden Slumbers', 4, "05:35");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 15, 'Carry That Weight', 4, "05:35");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 16, 'The End', 4, "05:35");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 17, 'Her Majesty', 4, "05:35");

INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 1, 'Shine On You Crazy Diamond (Parts 1-5)', 5, "13:32");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 2, 'Welcome To The Machine', 5, "07:31");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 3, 'Have A Cigar', 5, "05:07");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 4, 'Wish You Were Here', 5, "05:34");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('', '', 1, 5, 'Shine On You Crazy Diamond (Parts 6-9)', 5, "12:29");

INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('./resources/trackFiles/2-Dear Prudence (The Beatles)_v2.zip', './resources/trackFiles/2-Dear Prudence (The Beatles)_v2.mp3', 1, 1, 'Dear Prudence (The Beatles) piano cover', 6, "00:42");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('./resources/trackFiles/3-Isolation (John Lennon)_v1.zip', './resources/trackFiles/3-Isolation (John Lennon)_v1.mp3', 1, 2, 'Isolation (John Lennon) piano cover', 6, "02:20");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('./resources/trackFiles/5-Good night (The Beatles)_v1.zip', './resources/trackFiles/5-Good night (The Beatles)_v1.mp3', 1, 3, 'Good night (The Beatles) piano cover', 6, "02:50");
INSERT INTO `track` (zip_file, track_file, cd_id, track_id, name, album_id, track_length) VALUES ('./resources/trackFiles/6-Love (John Lennon)_v1.zip', './resources/trackFiles/6-Love (John Lennon)_v1.mp3', 1, 4, 'Love (John Lennon) piano cover', 6, "02:17");

INSERT INTO `album_favorite` (member_id, album_id) VALUES ('1', '3');
INSERT INTO `album_favorite` (member_id, album_id) VALUES ('1', '4');
INSERT INTO `album_favorite` (member_id, album_id) VALUES ('1', '5');
INSERT INTO `album_favorite` (member_id, album_id) VALUES ('2', '1');
INSERT INTO `album_favorite` (member_id, album_id) VALUES ('2', '4');

INSERT INTO `album_purchase` (member_id, album_id) VALUES ('1', '1');
INSERT INTO `album_purchase` (member_id, album_id) VALUES ('1', '2');
INSERT INTO `album_purchase` (member_id, album_id) VALUES ('1', '6');
INSERT INTO `album_purchase` (member_id, album_id) VALUES ('2', '3');
INSERT INTO `album_purchase` (member_id, album_id) VALUES ('2', '4');

INSERT INTO `track_favorite` (member_id, track_id) VALUES ('1', '25');
INSERT INTO `track_favorite` (member_id, track_id) VALUES ('1', '34');
INSERT INTO `track_favorite` (member_id, track_id) VALUES ('1', '58');
INSERT INTO `track_favorite` (member_id, track_id) VALUES ('2', '44');
INSERT INTO `track_favorite` (member_id, track_id) VALUES ('2', '53');

INSERT INTO `track_purchase` (member_id, track_id) VALUES ('1', '29');
INSERT INTO `track_purchase` (member_id, track_id) VALUES ('1', '39');
INSERT INTO `track_purchase` (member_id, track_id) VALUES ('1', '59');
INSERT INTO `track_purchase` (member_id, track_id) VALUES ('2', '42');
INSERT INTO `track_purchase` (member_id, track_id) VALUES ('2', '49');