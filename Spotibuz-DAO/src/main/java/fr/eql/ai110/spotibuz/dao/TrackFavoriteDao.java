package fr.eql.ai110.spotibuz.dao;

import java.util.LinkedHashSet;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;
import fr.eql.ai110.spotibuz.entity.Member;
import fr.eql.ai110.spotibuz.entity.TrackFavorite;
import fr.eql.ai110.spotibuz.idao.TrackFavoriteIDao;

@Remote(TrackFavoriteIDao.class)
@Stateless
public class TrackFavoriteDao extends GenericDao<TrackFavorite> implements TrackFavoriteIDao {

	@SuppressWarnings("unchecked")
	@Override
	public LinkedHashSet<TrackFavorite> findTracksFavoritesByMember(Member member) {
        Query query = em.createQuery("SELECT d FROM TrackFavorite d WHERE d.member = :memberParam");
        query.setParameter("memberParam", member);
        return new LinkedHashSet<TrackFavorite>(query.getResultList());
	}
}
