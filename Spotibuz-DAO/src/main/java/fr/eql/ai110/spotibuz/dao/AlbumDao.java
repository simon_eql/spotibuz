package fr.eql.ai110.spotibuz.dao;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.spotibuz.entity.Album;
import fr.eql.ai110.spotibuz.entity.AlbumFavorite;
import fr.eql.ai110.spotibuz.entity.AlbumPurchase;
import fr.eql.ai110.spotibuz.entity.Artist;
import fr.eql.ai110.spotibuz.idao.AlbumIDao;

@Remote(AlbumIDao.class)
@Stateless
public class AlbumDao extends GenericDao<Album> implements AlbumIDao {

	@SuppressWarnings("unchecked")
	@Override
	public Album authenticate(String name) {
		Album album = null;
		List<Album> albums;
		Query query = em.createQuery("SELECT a FROM Album a WHERE a.name = :nameParam");
		query.setParameter("nameParam", name);
		albums = query.getResultList();
		if (albums.size() > 0) {
			album = albums.get(0);
		}
		return album;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<Album> findAlbumsByConnectedArtist(Artist artist) {
		Query query = em.createQuery("SELECT a FROM Album a WHERE a.artist = :artistParam");
		query.setParameter("artistParam", artist);
		return new LinkedHashSet<Album>(query.getResultList());
	}

	@SuppressWarnings("unchecked")
	@Override
	public Album findAlbumByAlbumFavorite(AlbumFavorite albumFavorite) {
		Album album = null;
		List<Album> albums;
		Query query = em.createQuery("SELECT a FROM Album a WHERE a.id = :albumFavoriteParam");
		query.setParameter("albumFavoriteParam", albumFavorite.getAlbum().getId());
		albums = query.getResultList();
		if (albums.size() > 0) {
			album = albums.get(0);
		}
		return album;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Album findAlbumByAlbumPurchase(AlbumPurchase albumPurchase) {
		Album album = null;
		List<Album> albums;
		Query query = em.createQuery("SELECT a FROM Album a WHERE a.id = :albumPurchaseParam");
		query.setParameter("albumPurchaseParam", albumPurchase.getAlbum().getId());
		albums = query.getResultList();
		if (albums.size() > 0) {
			album = albums.get(0);
		}
		return album;
	}
}
