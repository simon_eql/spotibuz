package fr.eql.ai110.spotibuz.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.spotibuz.entity.Member;
import fr.eql.ai110.spotibuz.idao.MemberIDao;

@Remote(MemberIDao.class)
@Stateless
public class MemberDao extends GenericDao<Member> implements MemberIDao {

	@SuppressWarnings("unchecked")
	@Override
	public Member authenticate(String login, String password) {
		Member member = null;
		List<Member> members;
		Query query = em.createQuery("SELECT m FROM Member m WHERE m.login = :loginParam "
				+ "AND m.password = :passwordParam");
		query.setParameter("loginParam", login);
		query.setParameter("passwordParam", password);
		members = query.getResultList();
		if (members.size() > 0) {
			member = members.get(0);
		}
		return member;
	}

	@Override
	public Long findNbMembers() {
		Query query = em.createQuery("SELECT COUNT(m) FROM Member m");
		return (Long) query.getSingleResult();
	}
}
