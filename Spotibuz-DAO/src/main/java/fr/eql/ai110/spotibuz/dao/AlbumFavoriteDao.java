package fr.eql.ai110.spotibuz.dao;

import java.util.LinkedHashSet;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.spotibuz.entity.Member;
import fr.eql.ai110.spotibuz.entity.AlbumFavorite;
import fr.eql.ai110.spotibuz.idao.AlbumFavoriteIDao;

@Remote(AlbumFavoriteIDao.class)
@Stateless
public class AlbumFavoriteDao extends GenericDao<AlbumFavorite> implements AlbumFavoriteIDao {

	@SuppressWarnings("unchecked")
	@Override
    public LinkedHashSet<AlbumFavorite> findAlbumsFavoritesByMember(Member member) {
        Query query = em.createQuery("SELECT b FROM AlbumFavorite b WHERE b.member = :memberParam");
        query.setParameter("memberParam", member);
        return new LinkedHashSet<AlbumFavorite>(query.getResultList());
    }
}
