package fr.eql.ai110.spotibuz.dao;

import java.util.LinkedHashSet;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.spotibuz.entity.Member;
import fr.eql.ai110.spotibuz.entity.TrackPurchase;
import fr.eql.ai110.spotibuz.idao.TrackPurchaseIDao;

@Remote(TrackPurchaseIDao.class)
@Stateless
public class TrackPurchaseDao extends GenericDao<TrackPurchase> implements TrackPurchaseIDao {

	@SuppressWarnings("unchecked")
	@Override
	public LinkedHashSet<TrackPurchase> findTracksPurchaseByMember(Member member) {
        Query query = em.createQuery("SELECT e FROM TrackPurchase e WHERE e.member = :memberParam");
        query.setParameter("memberParam", member);
        return new LinkedHashSet<TrackPurchase>(query.getResultList());
	}

}
