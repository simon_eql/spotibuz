package fr.eql.ai110.spotibuz.dao;

import java.util.LinkedHashSet;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.spotibuz.entity.Member;
import fr.eql.ai110.spotibuz.entity.AlbumPurchase;
import fr.eql.ai110.spotibuz.idao.AlbumPurchaseIDao;

@Remote(AlbumPurchaseIDao.class)
@Stateless
public class AlbumPurchaseDao extends GenericDao<AlbumPurchase> implements AlbumPurchaseIDao {

	@SuppressWarnings("unchecked")
	@Override
    public LinkedHashSet<AlbumPurchase> findAlbumsPurchaseByMember(Member member) {
        Query query = em.createQuery("SELECT c FROM AlbumPurchase c WHERE c.member = :memberParam");
        query.setParameter("memberParam", member);
        return new LinkedHashSet<AlbumPurchase>(query.getResultList());
    }
}
