package fr.eql.ai110.spotibuz.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.spotibuz.entity.Artist;
import fr.eql.ai110.spotibuz.idao.ArtistIDao;

@Remote(ArtistIDao.class)
@Stateless
public class ArtistDao extends GenericDao<Artist> implements ArtistIDao {
	
	@SuppressWarnings("unchecked")
	@Override
	public Artist authenticate(String name) {
		Artist artist = null;
		List<Artist> artists;
		Query query = em.createQuery("SELECT a FROM Artist a WHERE a.name = :nameParam");
		query.setParameter("nameParam", name);
		artists = query.getResultList();
		if (artists.size() > 0) {
			artist = artists.get(0);
		}
		return artist;
	}
}
