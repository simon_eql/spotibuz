package fr.eql.ai110.spotibuz.dao;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.spotibuz.entity.Album;
import fr.eql.ai110.spotibuz.entity.Track;
import fr.eql.ai110.spotibuz.entity.TrackFavorite;
import fr.eql.ai110.spotibuz.entity.TrackPurchase;
import fr.eql.ai110.spotibuz.idao.TrackIDao;

@Remote(TrackIDao.class)
@Stateless
public class TrackDao extends GenericDao<Track> implements TrackIDao {
	
	@SuppressWarnings("unchecked")
	@Override
	public Track authenticate(String name) {
		Track trackSearch = null;
		List<Track> tracksSearch;
		Query query = em.createQuery("SELECT t FROM Track t WHERE t.name = :nameParam");
		query.setParameter("nameParam", name);
		tracksSearch = query.getResultList();
		if (tracksSearch.size() > 0) {
			trackSearch = tracksSearch.get(0);
		}
		return trackSearch;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<Track> findTracksByConnectedAlbum(Album album) {
		Query query = em.createQuery("SELECT t FROM Track t WHERE t.album = :albumParam");
		query.setParameter("albumParam", album);
		return new LinkedHashSet<Track>(query.getResultList());
	}

	@SuppressWarnings("unchecked")
	@Override
	public Track findTrackByTrackFavorite(TrackFavorite trackFavorite) {
		Track track = null;
		List<Track> tracks;
		Query query = em.createQuery("SELECT t FROM Track t WHERE t.id = :trackFavoriteParam");
		query.setParameter("trackFavoriteParam", trackFavorite.getTrack().getId());
		tracks = query.getResultList();
		if (tracks.size() > 0) {
			track = tracks.get(0);
		}
		return track;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Track findTrackByAlbumPurchase(TrackPurchase trackPurchase) {
		Track track = null;
		List<Track> tracks;
		Query query = em.createQuery("SELECT t FROM Track t WHERE t.id = :trackPurchaseParam");
		query.setParameter("trackPurchaseParam", trackPurchase.getTrack().getId());
		tracks = query.getResultList();
		if (tracks.size() > 0) {
			track = tracks.get(0);
		}
		return track;
	}

}



