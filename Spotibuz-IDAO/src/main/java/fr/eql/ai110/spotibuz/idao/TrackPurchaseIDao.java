package fr.eql.ai110.spotibuz.idao;

import java.util.LinkedHashSet;

import fr.eql.ai110.spotibuz.entity.Member;
import fr.eql.ai110.spotibuz.entity.TrackPurchase;

public interface TrackPurchaseIDao extends GenericIDao<TrackPurchase> {

	LinkedHashSet<TrackPurchase> findTracksPurchaseByMember(Member member);
}
