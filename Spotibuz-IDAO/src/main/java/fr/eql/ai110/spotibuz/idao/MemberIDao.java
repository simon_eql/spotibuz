package fr.eql.ai110.spotibuz.idao;

import fr.eql.ai110.spotibuz.entity.Member;

public interface MemberIDao extends GenericIDao<Member> {

	Member authenticate(String login, String password);
	Long findNbMembers();
}
