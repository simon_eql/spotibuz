package fr.eql.ai110.spotibuz.idao;

import java.util.LinkedHashSet;

import fr.eql.ai110.spotibuz.entity.AlbumPurchase;
import fr.eql.ai110.spotibuz.entity.Member;

public interface AlbumPurchaseIDao extends GenericIDao<AlbumPurchase> {

	LinkedHashSet<AlbumPurchase> findAlbumsPurchaseByMember(Member member);
}
