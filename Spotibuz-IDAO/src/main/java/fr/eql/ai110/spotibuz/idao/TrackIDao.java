package fr.eql.ai110.spotibuz.idao;

import java.util.Set;

import fr.eql.ai110.spotibuz.entity.Album;
import fr.eql.ai110.spotibuz.entity.Track;
import fr.eql.ai110.spotibuz.entity.TrackFavorite;
import fr.eql.ai110.spotibuz.entity.TrackPurchase;

public interface TrackIDao extends GenericIDao<Track> {

	Set<Track> findTracksByConnectedAlbum(Album album);
	
	Track authenticate(String name);

	Track findTrackByTrackFavorite(TrackFavorite trackFavorite);

	Track findTrackByAlbumPurchase(TrackPurchase trackPurchase);
}
