package fr.eql.ai110.spotibuz.idao;

import java.util.LinkedHashSet;

import fr.eql.ai110.spotibuz.entity.Member;
import fr.eql.ai110.spotibuz.entity.TrackFavorite;

public interface TrackFavoriteIDao extends GenericIDao<TrackFavorite> {

	LinkedHashSet<TrackFavorite> findTracksFavoritesByMember(Member member);
}
