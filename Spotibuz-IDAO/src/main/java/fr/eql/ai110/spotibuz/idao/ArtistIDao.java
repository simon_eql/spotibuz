package fr.eql.ai110.spotibuz.idao;

import fr.eql.ai110.spotibuz.entity.Artist;

public interface ArtistIDao extends GenericIDao<Artist> {

	Artist authenticate(String name);
}
