package fr.eql.ai110.spotibuz.idao;

import java.util.LinkedHashSet;

import fr.eql.ai110.spotibuz.entity.AlbumFavorite;
import fr.eql.ai110.spotibuz.entity.Member;

public interface AlbumFavoriteIDao extends GenericIDao<AlbumFavorite> {

	LinkedHashSet<AlbumFavorite> findAlbumsFavoritesByMember(Member member);
}
