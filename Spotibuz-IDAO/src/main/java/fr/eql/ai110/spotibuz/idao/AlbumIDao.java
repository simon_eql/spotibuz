package fr.eql.ai110.spotibuz.idao;

import java.util.Set;

import fr.eql.ai110.spotibuz.entity.Album;
import fr.eql.ai110.spotibuz.entity.AlbumFavorite;
import fr.eql.ai110.spotibuz.entity.AlbumPurchase;
import fr.eql.ai110.spotibuz.entity.Artist;

public interface AlbumIDao extends GenericIDao<Album> {

	Set<Album> findAlbumsByConnectedArtist(Artist artist);
	
	Album authenticate(String name);

	Album findAlbumByAlbumFavorite(AlbumFavorite albumFavorite);

	Album findAlbumByAlbumPurchase(AlbumPurchase albumPurchase);
}
