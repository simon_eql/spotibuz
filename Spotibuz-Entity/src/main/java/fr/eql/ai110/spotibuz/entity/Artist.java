package fr.eql.ai110.spotibuz.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="artist")
public class Artist implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="name")
	private String name;
	@Column(name="photo")
	private String photo;
	@Column(name="active_years")
	private String activeYears;
	@Column(name="nb_albums")
	private Integer nbAlbums;
	
	@OneToMany(mappedBy = "artist", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Album> albums;
	
	public Artist() {
		
	}
	
	public Artist(Integer id, String name, String photo, String activeYears, Integer nbAlbums, Set<Album> albums) {
		super();
		this.id = id;
		this.name = name;
		this.photo = photo;
		this.activeYears = activeYears;
		this.nbAlbums = nbAlbums;
		this.albums = albums;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getActiveYears() {
		return activeYears;
	}

	public void setActiveYears(String activeYears) {
		this.activeYears = activeYears;
	}

	public Integer getNbAlbums() {
		return nbAlbums;
	}

	public void setNbAlbums(Integer nbAlbums) {
		this.nbAlbums = nbAlbums;
	}

	public Set<Album> getAlbums() {
		return albums;
	}

	public void setAlbums(Set<Album> albums) {
		this.albums = albums;
	}
}