package fr.eql.ai110.spotibuz.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="album")
public class Album implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="name")
	private String name;
	@Column(name="photo")
	private String photo;
	@Column(name="release_date")
	private String releaseDate;
	@Column(name="nb_tracks")
	private Integer nbTracks;
	@Column(name="album_length")
	private String albumLength;
	@Column(name="sampling_frequency")
	private Float samplingFrequency;
	@Column(name="dynamic_range")
	private Integer dynamicRange;
	@Column(name="zip_file")
	private String zipFile;

	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Artist artist;
	
	@OneToMany(mappedBy = "album", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<AlbumFavorite> albumFavorites;
	
	@OneToMany(mappedBy = "album", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<AlbumPurchase> albumPurchases;
	
	@OneToMany(mappedBy = "album", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Track> tracks;

	public Album() {
		
	}

	public Album(Integer id, String name, String photo, String releaseDate, Integer nbTracks, String albumLength,
			Float samplingFrequency, Integer dynamicRange, String zipFile, Artist artist,
			Set<AlbumFavorite> albumFavorites, Set<AlbumPurchase> albumPurchases, Set<Track> tracks) {
		super();
		this.id = id;
		this.name = name;
		this.photo = photo;
		this.releaseDate = releaseDate;
		this.nbTracks = nbTracks;
		this.albumLength = albumLength;
		this.samplingFrequency = samplingFrequency;
		this.dynamicRange = dynamicRange;
		this.zipFile = zipFile;
		this.artist = artist;
		this.albumFavorites = albumFavorites;
		this.albumPurchases = albumPurchases;
		this.tracks = tracks;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Integer getNbTracks() {
		return nbTracks;
	}

	public void setNbTracks(Integer nbTracks) {
		this.nbTracks = nbTracks;
	}

	public String getAlbumLength() {
		return albumLength;
	}

	public void setAlbumLength(String albumLength) {
		this.albumLength = albumLength;
	}

	public Float getSamplingFrequency() {
		return samplingFrequency;
	}

	public void setSamplingFrequency(Float samplingFrequency) {
		this.samplingFrequency = samplingFrequency;
	}

	public Integer getDynamicRange() {
		return dynamicRange;
	}

	public void setDynamicRange(Integer dynamicRange) {
		this.dynamicRange = dynamicRange;
	}

	public Artist getArtist() {
		return artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}

	public Set<AlbumFavorite> getAlbumFavorites() {
		return albumFavorites;
	}

	public void setAlbumFavorites(Set<AlbumFavorite> albumFavorites) {
		this.albumFavorites = albumFavorites;
	}

	public Set<AlbumPurchase> getAlbumPurchases() {
		return albumPurchases;
	}

	public void setAlbumPurchases(Set<AlbumPurchase> albumPurchases) {
		this.albumPurchases = albumPurchases;
	}

	public Set<Track> getTracks() {
		return tracks;
	}

	public void setTracks(Set<Track> tracks) {
		this.tracks = tracks;
	}

	public String getZipFile() {
		return zipFile;
	}

	public void setZipFile(String zipFile) {
		this.zipFile = zipFile;
	}
	
}