package fr.eql.ai110.spotibuz.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="member")
public class Member implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="name")
	private String name;
	@Column(name="surname")
	private String surname;
	@Column(name="login")
	private String login;
	@Column(name="password")
	private String password;
	@Column(name="photo")
	private String photo;
	@Column(name="email")
	private String email;
	/*
	 * Cascade : Indique s'il faut faire cascader les opérations sur les objets associés, 
	 * et dans quel type d'opération.
	 * Fetch : indique si la requête doit faire remonter tout de suite (EAGER) 
	 * ou éventuellement (LAZY) les collections contenues dans l'objet.
	 */

    @OneToMany(mappedBy = "member", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<AlbumFavorite> albumFavorites;
    
    @OneToMany(mappedBy = "member", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<AlbumPurchase> albumPurchases;
    
    @OneToMany(mappedBy = "member", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<TrackFavorite> trackFavorites;
    
    @OneToMany(mappedBy = "member", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<TrackPurchase> trackPurchases;
	
	public Member() {
		
	}

	public Member(Integer id, String name, String surname, String login, String password, String photo, String email,
			Set<AlbumFavorite> albumFavorites, Set<AlbumPurchase> albumPurchases, Set<TrackFavorite> trackFavorites,
			Set<TrackPurchase> trackPurchases) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.login = login;
		this.password = password;
		this.photo = photo;
		this.email = email;
		this.albumFavorites = albumFavorites;
		this.albumPurchases = albumPurchases;
		this.trackFavorites = trackFavorites;
		this.trackPurchases = trackPurchases;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<AlbumFavorite> getAlbumFavorites() {
		return albumFavorites;
	}

	public void setAlbumFavorites(Set<AlbumFavorite> albumFavorites) {
		this.albumFavorites = albumFavorites;
	}

	public Set<AlbumPurchase> getAlbumPurchases() {
		return albumPurchases;
	}

	public void setAlbumPurchases(Set<AlbumPurchase> albumPurchases) {
		this.albumPurchases = albumPurchases;
	}

	public Set<TrackFavorite> getTrackFavorites() {
		return trackFavorites;
	}

	public void setTrackFavorites(Set<TrackFavorite> trackFavorites) {
		this.trackFavorites = trackFavorites;
	}

	public Set<TrackPurchase> getTrackPurchases() {
		return trackPurchases;
	}

	public void setTrackPurchases(Set<TrackPurchase> trackPurchases) {
		this.trackPurchases = trackPurchases;
	}
}