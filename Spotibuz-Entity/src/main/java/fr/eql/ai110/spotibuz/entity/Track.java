package fr.eql.ai110.spotibuz.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="track")
public class Track implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="name")
	private String name;
	@Column(name="track_id")
	private Integer trackId;	
	@Column(name="cd_id")
	private Integer cdId;	
	@Column(name="track_length")
	private String trackLength;
	@Column(name="track_file")
	private String trackFile;
	@Column(name="zip_file")
	private String zipFile;
	
	public String getTrackFile() {
		return trackFile;
	}

	public void setTrackFile(String trackFile) {
		this.trackFile = trackFile;
	}

	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Album album;
	
	@OneToMany(mappedBy = "track", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<TrackFavorite> trackFavorites;
	
	@OneToMany(mappedBy = "track", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<TrackPurchase> trackPurchases;
	
	public Track() {

	}



	public Track(Integer id, String name, Integer trackId, Integer cdId, String trackLength, String trackFile,
			String zipFile, Album album, Set<TrackFavorite> trackFavorites, Set<TrackPurchase> trackPurchases) {
		super();
		this.id = id;
		this.name = name;
		this.trackId = trackId;
		this.cdId = cdId;
		this.trackLength = trackLength;
		this.trackFile = trackFile;
		this.zipFile = zipFile;
		this.album = album;
		this.trackFavorites = trackFavorites;
		this.trackPurchases = trackPurchases;
	}

	public Set<TrackFavorite> getTrackFavorites() {
		return trackFavorites;
	}

	public void setTrackFavorites(Set<TrackFavorite> trackFavorites) {
		this.trackFavorites = trackFavorites;
	}

	public Set<TrackPurchase> getTrackPurchases() {
		return trackPurchases;
	}

	public void setTrackPurchases(Set<TrackPurchase> trackPurchases) {
		this.trackPurchases = trackPurchases;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getTrackId() {
		return trackId;
	}

	public void setTrackId(Integer trackId) {
		this.trackId = trackId;
	}

	public Integer getCdId() {
		return cdId;
	}

	public void setCdId(Integer cdId) {
		this.cdId = cdId;
	}

	public String getTrackLength() {
		return trackLength;
	}

	public void setTrackLength(String trackLength) {
		this.trackLength = trackLength;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public String getZipFile() {
		return zipFile;
	}

	public void setZipFile(String zipFile) {
		this.zipFile = zipFile;
	}
	
}