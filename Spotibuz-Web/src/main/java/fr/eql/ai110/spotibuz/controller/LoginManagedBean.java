package fr.eql.ai110.spotibuz.controller;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import fr.eql.ai110.spotibuz.entity.Member;
import fr.eql.ai110.spotibuz.ibusiness.AccountIBusiness;

@ManagedBean(name="mbLogin")
@SessionScoped
public class LoginManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String login;
	private String password;
	private String surname;
	private String name;
	private String email;
	private String photo;
	
	private Member member;
	
	@EJB
	private AccountIBusiness accountBusiness;


	public String create() {
		String forward = "/login.xhtml?faces-redirection=true";
		
		// instancier un member :
		Member member = new Member();
		member.setLogin(login);
		member.setName(name);
		member.setSurname(surname);
		member.setEmail(email);
		member.setPhoto(photo);
		member.setPassword(password);
		
		// enregistrer le nouvel member (business)
		accountBusiness.create(member, password);
		
		return forward;
	}
	
	public String connect() {
		String forward = null;
		member = accountBusiness.connect(login, password);
		if (isConnected()) {
			forward = "/index.xhtml?faces-redirection=true";
		} else {
			FacesMessage facesMessage = new FacesMessage(
					FacesMessage.SEVERITY_WARN,
					"Identifiant et/ou mot de passe incorrect(s)",
					"Identifiant et/ou mot de passe incorrect(s)"
					);
			FacesContext.getCurrentInstance().addMessage("loginForm:inpLogin", facesMessage);
			FacesContext.getCurrentInstance().addMessage("loginForm:inpPassword", facesMessage);
			forward = "/login.xhtml?faces-redirection=false";
		}
		return forward;
	}
	
	public boolean isConnected() {
		return member != null;
	}
	
	public String disconnect() {
		HttpSession session = (HttpSession) FacesContext
				.getCurrentInstance()
				.getExternalContext()
				.getSession(true);
		session.invalidate();
		member = null;
		return "/login.xhtml?faces-redirection=true";
	}
	
	public String disconnectSignUp() {
		HttpSession session = (HttpSession) FacesContext
				.getCurrentInstance()
				.getExternalContext()
				.getSession(true);
		session.invalidate();
		member = null;
		return "";
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
		
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Member getMember() {
		return member;
	}
	public void setMember(Member member) {
		this.member = member;
	}

	public AccountIBusiness getAccountBusiness() {
		return accountBusiness;
	}

	public void setAccountBusiness(AccountIBusiness accountBusiness) {
		this.accountBusiness = accountBusiness;
	}
	
}
