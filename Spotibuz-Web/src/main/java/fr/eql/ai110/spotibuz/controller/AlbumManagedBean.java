package fr.eql.ai110.spotibuz.controller;

import java.io.Serializable;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import fr.eql.ai110.spotibuz.entity.Album;
import fr.eql.ai110.spotibuz.entity.AlbumFavorite;
import fr.eql.ai110.spotibuz.entity.Artist;
import fr.eql.ai110.spotibuz.entity.Member;
import fr.eql.ai110.spotibuz.entity.Track;
import fr.eql.ai110.spotibuz.ibusiness.AlbumIBusiness;
import fr.eql.ai110.spotibuz.ibusiness.ArtistIBusiness;
import fr.eql.ai110.spotibuz.ibusiness.TrackIBusiness;

@ManagedBean(name="mbAlbum")
@RequestScoped
public class AlbumManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManagedProperty(value="#{mbLogin.member}")
	private Member connectedMember;

	@ManagedProperty(value="#{mbArtist.artist}")
	private Artist connectedArtist;
	
	private Set<Album> connectedMemberFavoritesAlbums;
	private Set<Track> connectedMemberFavoritesTracks;
	
	private Set<Album> connectedMemberPurchasesAlbums;
	private Set<Track> connectedMemberPurchasesTracks;

	private Set<Album> connectedArtistAlbums;

	private Set<AlbumFavorite> albumFavoritesByMember;
	
	private Member member;

	private String name;
	private String repeatAlbumName;

	private Integer nbTracks;
	private String albumLength;
	private Float samplingFrequency;
	private Integer dynamicRange;
	private Set<Track> tracks;

	private Album album;
	private Integer id;

	@EJB
	private ArtistIBusiness artistBusiness;
	@EJB
	private AlbumIBusiness albumBusiness;
	@EJB
	private TrackIBusiness trackBusiness;

	public String create() {
		String forward = "/album.xhtml?faces-redirection=true";

		// instancier un album :
		Album album = new Album();
		album.setName(name);
		album.setNbTracks(nbTracks);
		album.setAlbumLength(albumLength);
		album.setSamplingFrequency(samplingFrequency);
		album.setDynamicRange(dynamicRange);
		album.setTracks(tracks);

		// enregistrer le nouvel album (business)
		albumBusiness.create(album);

		return forward;
	}

	public String connect() {
		String forward = null;
		album = albumBusiness.connect(name);
		if (isConnected()) {
			forward = "/album.xhtml?faces-redirection=true";
		} else {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Album non présent dans le catalogue Spotibuz", null);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, message);  
			forward = "/search.xhtml?faces-redirection=false";
		}
		return forward;
	}

	public String connectFromArtist(Album someAlbum) {
		String forward = null;
		album = albumBusiness.connect(someAlbum.getName());
		if (album != null) {
			forward = "/album.xhtml?faces-redirection=true";
		} else {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Album non présent dans le catalogue Spotibuz", null);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, message);  
			forward = "/search.xhtml?faces-redirection=false";
		}
		return forward;
	} 

	public boolean isConnected() {
		return album != null;
	}

	public String submit() {
		// do work
		return "album.xhtml?faces-redirect=true";
	}

	@PostConstruct
	public void init() {
		connectedMemberFavoritesAlbums = albumBusiness.getFavoritesAlbumsByMember(connectedMember);
		connectedMemberFavoritesTracks = trackBusiness.getFavoritesTracksByMember(connectedMember);
		
		connectedMemberPurchasesAlbums = albumBusiness.getPurchasesAlbumsByMember(connectedMember);
		connectedMemberPurchasesTracks = trackBusiness.getPurchasesTracksByMember(connectedMember);
		
		connectedArtistAlbums = albumBusiness.getAlbumsByConnectedArtist(connectedArtist);
	}

	public AlbumIBusiness getAlbumBusiness() {
		return albumBusiness;
	}

	public void setAlbumBusiness(AlbumIBusiness albumBusiness) {
		this.albumBusiness = albumBusiness;
	}

	public Member getConnectedMember() {
		return connectedMember;
	}
	public void setConnectedMember(Member connectedMember) {
		this.connectedMember = connectedMember;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNbTracks() {
		return nbTracks;
	}

	public void setNbTracks(Integer nbTracks) {
		this.nbTracks = nbTracks;
	}

	public String getAlbumLength() {
		return albumLength;
	}

	public void setAlbumLength(String albumLength) {
		this.albumLength = albumLength;
	}

	public Float getSamplingFrequency() {
		return samplingFrequency;
	}

	public void setSamplingFrequency(Float samplingFrequency) {
		this.samplingFrequency = samplingFrequency;
	}

	public Integer getDynamicRange() {
		return dynamicRange;
	}

	public void setDynamicRange(Integer dynamicRange) {
		this.dynamicRange = dynamicRange;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public TrackIBusiness getTrackBusiness() {
		return trackBusiness;
	}

	public void setTrackBusiness(TrackIBusiness trackBusiness) {
		this.trackBusiness = trackBusiness;
	}

	public Set<Album> getConnectedArtistAlbums() {
		return connectedArtistAlbums;
	}

	public void setConnectedArtistAlbums(Set<Album> connectedArtistAlbums) {
		this.connectedArtistAlbums = connectedArtistAlbums;
	}

	public Artist getConnectedArtist() {
		return connectedArtist;
	}

	public void setConnectedArtist(Artist connectedArtist) {
		this.connectedArtist = connectedArtist;
	}

	public Set<Track> getTracks() {
		return tracks;
	}

	public void setTracks(Set<Track> tracks) {
		this.tracks = tracks;
	}

	public String getRepeatAlbumName() {
		return repeatAlbumName;
	}

	public void setRepeatAlbumName(String repeatAlbumName) {
		this.repeatAlbumName = repeatAlbumName;
	}

	public Set<Album> getConnectedMemberFavoritesAlbums() {
		return connectedMemberFavoritesAlbums;
	}

	public void setConnectedMemberFavoritesAlbums(Set<Album> connectedMemberFavoritesAlbums) {
		this.connectedMemberFavoritesAlbums = connectedMemberFavoritesAlbums;
	}

	public Set<Album> getConnectedMemberPurchasesAlbums() {
		return connectedMemberPurchasesAlbums;
	}

	public void setConnectedMemberPurchasesAlbums(Set<Album> connectedMemberPurchasesAlbums) {
		this.connectedMemberPurchasesAlbums = connectedMemberPurchasesAlbums;
	}

	public Set<AlbumFavorite> getAlbumFavoritesByMember() {
		return albumFavoritesByMember;
	}

	public void setAlbumFavoritesByMember(Set<AlbumFavorite> albumFavoritesByMember) {
		this.albumFavoritesByMember = albumFavoritesByMember;
	}

	public Set<Track> getConnectedMemberFavoritesTracks() {
		return connectedMemberFavoritesTracks;
	}

	public void setConnectedMemberFavoritesTracks(Set<Track> connectedMemberFavoritesTracks) {
		this.connectedMemberFavoritesTracks = connectedMemberFavoritesTracks;
	}

	public Set<Track> getConnectedMemberPurchasesTracks() {
		return connectedMemberPurchasesTracks;
	}

	public void setConnectedMemberPurchasesTracks(Set<Track> connectedMemberPurchasesTracks) {
		this.connectedMemberPurchasesTracks = connectedMemberPurchasesTracks;
	}

	public ArtistIBusiness getArtistBusiness() {
		return artistBusiness;
	}

	public void setArtistBusiness(ArtistIBusiness artistBusiness) {
		this.artistBusiness = artistBusiness;
	}

}
