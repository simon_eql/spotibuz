package fr.eql.ai110.spotibuz.controller;

import java.io.Serializable;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import fr.eql.ai110.spotibuz.entity.Album;
import fr.eql.ai110.spotibuz.entity.Member;
import fr.eql.ai110.spotibuz.entity.Track;
import fr.eql.ai110.spotibuz.ibusiness.TrackIBusiness;

@ManagedBean(name="mbTrack")
@RequestScoped
public class TrackManagedBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value="#{mbLogin.member}")
	private Member connectedMember;
	
	@ManagedProperty(value="#{mbAlbum.album}")
	private Album connectedAlbum;
	
	private Set<Track> connectedMemberFavoritesTracks;
	
	private Set<Track> connectedMemberPurchasesTracks;
	
	private Set<Track> connectedAlbumTracks;
	
	private Member member;
	
	private String name;
	private Integer trackId;
	private Integer cdId;	
	private String trackLength;
	
	private Track track;
	
	@EJB
	private TrackIBusiness trackBusiness;

	public String create() {
		String forward = "/track.xhtml?faces-redirection=true";
		
		// instancier une piste :
		Track track = new Track();
		track.setName(name);
		track.setTrackId(trackId);
		track.setCdId(cdId);
		track.setTrackLength(trackLength);
		
		// enregistrer le nouvel album (business)
		trackBusiness.create(track);
		
		return forward;
	}
	
	public String connect() {
		String forward = null;
		track = trackBusiness.connect(name);
		if (isConnected()) {
			forward = "/track.xhtml?faces-redirection=true";
		} else {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Piste non présente dans le catalogue Spotibuz", null);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, message);  
			forward = "/search.xhtml?faces-redirection=false";
		}
		return forward;
	}
	
	public boolean isConnected() {
		return track != null;
	}
	
	@PostConstruct
	public void init() {
		connectedAlbumTracks = trackBusiness.getTracksByConnectedAlbum(connectedAlbum);
	}

	public Album getConnectedAlbum() {
		return connectedAlbum;
	}

	public void setConnectedAlbum(Album connectedAlbum) {
		this.connectedAlbum = connectedAlbum;
	}

	public Set<Track> getConnectedAlbumTracks() {
		return connectedAlbumTracks;
	}

	public void setConnectedAlbumTracks(Set<Track> connectedAlbumTracks) {
		this.connectedAlbumTracks = connectedAlbumTracks;
	}

	public TrackIBusiness getTrackBusiness() {
		return trackBusiness;
	}

	public void setTrackBusiness(TrackIBusiness trackBusiness) {
		this.trackBusiness = trackBusiness;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getTrackId() {
		return trackId;
	}

	public void setTrackId(Integer trackId) {
		this.trackId = trackId;
	}

	public Integer getCdId() {
		return cdId;
	}

	public void setCdId(Integer cdId) {
		this.cdId = cdId;
	}

	public String getTrackLength() {
		return trackLength;
	}

	public void setTrackLength(String trackLength) {
		this.trackLength = trackLength;
	}

	public Track getTrack() {
		return track;
	}

	public void setTrack(Track track) {
		this.track = track;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Set<Track> getConnectedMemberFavoritesTracks() {
		return connectedMemberFavoritesTracks;
	}

	public void setConnectedMemberFavoritesTracks(Set<Track> connectedMemberFavoritesTracks) {
		this.connectedMemberFavoritesTracks = connectedMemberFavoritesTracks;
	}

	public Member getConnectedMember() {
		return connectedMember;
	}

	public void setConnectedMember(Member connectedMember) {
		this.connectedMember = connectedMember;
	}

	public Set<Track> getConnectedMemberPurchasesTracks() {
		return connectedMemberPurchasesTracks;
	}

	public void setConnectedMemberPurchasesTracks(Set<Track> connectedMemberPurchasesTracks) {
		this.connectedMemberPurchasesTracks = connectedMemberPurchasesTracks;
	}
	
}
