package fr.eql.ai110.spotibuz.controller;

import java.io.Serializable;
import java.util.Set;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import fr.eql.ai110.spotibuz.entity.Album;
import fr.eql.ai110.spotibuz.entity.Artist;
import fr.eql.ai110.spotibuz.entity.Member;
import fr.eql.ai110.spotibuz.ibusiness.ArtistIBusiness;

@ManagedBean(name="mbArtist")
@SessionScoped
public class ArtistManagedBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value="#{mbLogin.member}")

	private Member member;
	
	private Artist artist;

	private Integer id;
	private String name;
	private String photo;
	private String activeYears;
	private Integer nbAlbums;
	private Set<Album> albums;
	
	@EJB
	private ArtistIBusiness artistBusiness;
	
	public String create() {
		String forward = "/artist.xhtml?faces-redirection=true";
		
		// instancier un artiste :
		Artist artist = new Artist();
		artist.setName(name);
		artist.setPhoto(photo);
		artist.setActiveYears(activeYears);
		artist.setNbAlbums(nbAlbums);
		artist.setAlbums(albums);
		
		// enregistrer le nouvel artiste (business)
		artistBusiness.create(artist);
		
		return forward;
	}
	
	public String connect() {
		String forward = null;
		artist = artistBusiness.connect(name);
		if (isConnected()) {
			forward = "/artist.xhtml?faces-redirection=true";
		} else {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Artiste non présent(e) dans le catalogue Spotibuz", null);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, message);  
			forward = "/search.xhtml?faces-redirection=false";
		}
		return forward;
	}
	
	public Artist getArtist() {
		return artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getActiveYears() {
		return activeYears;
	}

	public void setActiveYears(String activeYears) {
		this.activeYears = activeYears;
	}

	public Integer getNbAlbums() {
		return nbAlbums;
	}

	public void setNbAlbums(Integer nbAlbums) {
		this.nbAlbums = nbAlbums;
	}

	public Set<Album> getAlbums() {
		return albums;
	}

	public void setAlbums(Set<Album> albums) {
		this.albums = albums;
	}
	
	public boolean isConnected() {
		return artist != null;
	}
	
	public boolean isBookmarked() {
		return member != null;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ArtistIBusiness getArtistBusiness() {
		return artistBusiness;
	}

	public void setArtistBusiness(ArtistIBusiness artistBusiness) {
		this.artistBusiness = artistBusiness;
	}
}
