package fr.eql.ai110.spotibuz.ibusiness;

import java.util.Set;

import fr.eql.ai110.spotibuz.entity.Album;
import fr.eql.ai110.spotibuz.entity.Member;
import fr.eql.ai110.spotibuz.entity.Track;

public interface TrackIBusiness {
	
	Set<Track> getTracksByConnectedAlbum(Album connectedAlbum);
	
	void create(Track track);
	
	public Track connect(String name);

	Set<Track> getFavoritesTracksByMember(Member connectedMember);

	Set<Track> getPurchasesTracksByMember(Member connectedMember);
	
}
