package fr.eql.ai110.spotibuz.ibusiness;

import java.util.Set;

import fr.eql.ai110.spotibuz.entity.Album;
import fr.eql.ai110.spotibuz.entity.Artist;
import fr.eql.ai110.spotibuz.entity.Member;

public interface AlbumIBusiness {

	Set<Album> getAlbumsByConnectedArtist(Artist connectedArtist);
	
	void create(Album album);

	public Album connect(String name);

	Set<Album> getFavoritesAlbumsByMember(Member connectedMember);

	Set<Album> getPurchasesAlbumsByMember(Member connectedMember);
}
