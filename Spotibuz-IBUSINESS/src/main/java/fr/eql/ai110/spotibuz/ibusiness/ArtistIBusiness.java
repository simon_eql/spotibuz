package fr.eql.ai110.spotibuz.ibusiness;

import fr.eql.ai110.spotibuz.entity.Artist;

public interface ArtistIBusiness {
	void create(Artist artist);

	Artist connect(String name);
}
