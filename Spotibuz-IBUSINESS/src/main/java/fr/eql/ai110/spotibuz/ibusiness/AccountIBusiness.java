package fr.eql.ai110.spotibuz.ibusiness;

import fr.eql.ai110.spotibuz.entity.Member;

public interface AccountIBusiness {
	
	public Member connect(String login, String password);

	public void create(Member member, String password);

}
