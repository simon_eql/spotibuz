package fr.eql.ai110.spotibuz.business;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.spotibuz.entity.Member;
import fr.eql.ai110.spotibuz.ibusiness.AccountIBusiness;
import fr.eql.ai110.spotibuz.idao.MemberIDao;

@Remote(AccountIBusiness.class)
@Stateless
public class AccountBusiness implements AccountIBusiness {

	@EJB
	private MemberIDao memberDao;
	
	@Override
	public Member connect(String login, String password) {
		return memberDao.authenticate(login, password);
	}

	@Override
	public void create(Member member, String password) {
	
		memberDao.add(member);
	}
}
