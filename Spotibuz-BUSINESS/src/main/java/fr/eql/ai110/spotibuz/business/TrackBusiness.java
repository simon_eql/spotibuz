package fr.eql.ai110.spotibuz.business;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.spotibuz.entity.Album;
import fr.eql.ai110.spotibuz.entity.Member;
import fr.eql.ai110.spotibuz.entity.Track;
import fr.eql.ai110.spotibuz.entity.TrackFavorite;
import fr.eql.ai110.spotibuz.entity.TrackPurchase;
import fr.eql.ai110.spotibuz.ibusiness.TrackIBusiness;
import fr.eql.ai110.spotibuz.idao.TrackFavoriteIDao;
import fr.eql.ai110.spotibuz.idao.TrackIDao;
import fr.eql.ai110.spotibuz.idao.TrackPurchaseIDao;

@Remote(TrackIBusiness.class)
@Stateless
public class TrackBusiness implements TrackIBusiness {
	
	@EJB
	TrackIDao trackDao;
	@EJB
	TrackFavoriteIDao trackFavoriteDao;
	@EJB
	TrackPurchaseIDao trackPurchaseDao;

	@Override
	public Set<Track> getTracksByConnectedAlbum(Album album) {
		return trackDao.findTracksByConnectedAlbum(album);
	}
	
	@Override
	public void create(Track track) {
	
		trackDao.add(track);
	}

	@Override
	public Track connect(String name) {
		return trackDao.authenticate(name);
	}

	@Override
	public Set<Track> getFavoritesTracksByMember(Member member) {
		LinkedHashSet<Track> tracks = new LinkedHashSet<Track>();
		LinkedHashSet<TrackFavorite> trackFavorites = new LinkedHashSet<TrackFavorite>();
		trackFavorites = (LinkedHashSet<TrackFavorite>) trackFavoriteDao.findTracksFavoritesByMember(member);
		System.out.println("trackFavoriteDao : " + trackFavorites.size());
		for (TrackFavorite trackFavorite : trackFavorites) {
			tracks.add(trackDao.findTrackByTrackFavorite(trackFavorite));
		}
		return tracks; 
	} 

	@Override
	public Set<Track> getPurchasesTracksByMember(Member member) {
		LinkedHashSet<Track> tracks = new LinkedHashSet<Track>();
		LinkedHashSet<TrackPurchase> trackPurchases = new LinkedHashSet<TrackPurchase>();
		trackPurchases = (LinkedHashSet<TrackPurchase>) trackPurchaseDao.findTracksPurchaseByMember(member);
		System.out.println("trackPurchaseDao : " + trackPurchases.size());
		for (TrackPurchase trackPurchase : trackPurchases) {
			tracks.add(trackDao.findTrackByAlbumPurchase(trackPurchase));
		}
		return tracks; 
	} 
}