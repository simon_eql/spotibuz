package fr.eql.ai110.spotibuz.business;


import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import fr.eql.ai110.spotibuz.entity.Artist;
import fr.eql.ai110.spotibuz.ibusiness.ArtistIBusiness;
import fr.eql.ai110.spotibuz.idao.ArtistIDao;

@Remote(ArtistIBusiness.class)
@Stateless
public class ArtistBusiness implements ArtistIBusiness {
	
	@EJB
	ArtistIDao artistDao;
	
	@Override
	public void create(Artist artist) {
		artistDao.add(artist);
	}
	
	@Override
	public Artist connect(String name) {
		return artistDao.authenticate(name);
	}
//
//	@Override
//	public Set<Album> getArtistsByMemberFavorites(Member member) {
//		return albumDao.findAlbumsByMemberFavorites(member);
//	}
//	
//	@Override
//	public Set<Album> getArtistsByMemberPurchases(Member member) {
//		return albumDao.findAlbumsByMemberPurchases(member);
//	}
}
