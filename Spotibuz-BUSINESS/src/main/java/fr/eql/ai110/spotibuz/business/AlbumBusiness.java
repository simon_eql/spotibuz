package fr.eql.ai110.spotibuz.business;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.spotibuz.entity.Album;
import fr.eql.ai110.spotibuz.entity.AlbumFavorite;
import fr.eql.ai110.spotibuz.entity.AlbumPurchase;
import fr.eql.ai110.spotibuz.entity.Artist;
import fr.eql.ai110.spotibuz.entity.Member;
import fr.eql.ai110.spotibuz.ibusiness.AlbumIBusiness;
import fr.eql.ai110.spotibuz.idao.AlbumFavoriteIDao;
import fr.eql.ai110.spotibuz.idao.AlbumIDao;
import fr.eql.ai110.spotibuz.idao.AlbumPurchaseIDao;

@Remote(AlbumIBusiness.class)
@Stateless
public class AlbumBusiness implements AlbumIBusiness {

	@EJB
	AlbumIDao albumDao;
	@EJB
	AlbumFavoriteIDao albumFavoriteDao;
	@EJB
	AlbumPurchaseIDao albumPurchaseDao;

	@Override
	public void create(Album album) {

		albumDao.add(album);
	}

	@Override
	public Album connect(String name) {
		return albumDao.authenticate(name);
	}

	@Override
	public Set<Album> getAlbumsByConnectedArtist(Artist artist) {
		return albumDao.findAlbumsByConnectedArtist(artist);
	}

	@Override
	public Set<Album> getFavoritesAlbumsByMember(Member member) {
		LinkedHashSet<Album> albums = new LinkedHashSet<Album>();
		LinkedHashSet<AlbumFavorite> albumFavorites = new LinkedHashSet<AlbumFavorite>();
		albumFavorites = (LinkedHashSet<AlbumFavorite>) albumFavoriteDao.findAlbumsFavoritesByMember(member);
		System.out.println("albumFavoriteDao : " + albumFavorites.size());
		for (AlbumFavorite albumFavorite : albumFavorites) {
			albums.add(albumDao.findAlbumByAlbumFavorite(albumFavorite));
		}
		return albums; 
	} 

	@Override
	public Set<Album> getPurchasesAlbumsByMember(Member member) {
		LinkedHashSet<Album> albums = new LinkedHashSet<Album>();
		LinkedHashSet<AlbumPurchase> albumPurchases = new LinkedHashSet<AlbumPurchase>();
		albumPurchases = (LinkedHashSet<AlbumPurchase>) albumPurchaseDao.findAlbumsPurchaseByMember(member);
		System.out.println("albumPurchaseDao : " + albumPurchases.size());
		for (AlbumPurchase albumPurchase : albumPurchases) {
			albums.add(albumDao.findAlbumByAlbumPurchase(albumPurchase));
		}
		return albums; 
	} 


}
